/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Component, OnInit} from '@angular/core';
import {WaterService} from '../service/water.service';
import {Water} from '../model/water';
import {AuthService} from '../auth.service';


@Component({
  selector: 'app-water',
  templateUrl: './water.component.html',
  styleUrls: ['./water.component.css']
})
export class WaterComponent implements OnInit {

  model = new Water;
  url:any;

  constructor(public waterService: WaterService, public auth: AuthService) {

  }

  ngOnInit() {
    this.loadData();
    this.url = this.waterService.getUrl();
  }
  refresh(newItem: string) {
    this.loadData();
    this.refreshChart();
  }

  onSubmit() {
    this.model.userId = this.auth.getUserId();
    console.log("POST water to backend : " /*+ JSON.stringify(this.model)*/);
    this.waterService.add(this.model);
    this.refreshChart();
  }

  delete(water: Water): void {
    this.waterService.delete(water);
    this.refreshChart();
  }

  refreshChart() {
    this.waterService.getChart(this.auth.getUserId());
  }

  loadPage(page: number) {
    if (page !== this.waterService.previousPage) {
      this.waterService.previousPage = page;
      this.loadData();
    }
  }

  loadData() {
    if(this.waterService.waterList.length == 0) {
      this.waterService.getAllPaginate(this.auth.getUserId(),
        this.waterService.page - 1,
        this.waterService.itemsPerPage,
      );
    }
  }
}
