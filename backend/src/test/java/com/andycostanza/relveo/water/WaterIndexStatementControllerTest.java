package com.andycostanza.relveo.water;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.utils.CsvFileMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class WaterIndexStatementControllerTest {
    WaterIndexStatementController controller;
    @Mock
    WaterIndexStatementRepository repository;
    @Mock
    ChartService chartService;
    @Mock
    CsvFileMapper csvFileMapper;
    @Mock
    WaterIndexStatementMapper mapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new WaterIndexStatementController(repository,chartService,
                                                       csvFileMapper,
                                                       mapper);
    }

    @Test
    void findAllPaginate() {
        //given
        Mockito.when(repository.findByUserId(Mockito.anyString(), Mockito.any(PageRequest.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(WaterIndexStatement.builder()
                        .id(1L)
                        .build())));
        //when
        ResponseEntity result = controller.findAllPaginate("userId", 1,
                1);
        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .findByUserId(Mockito.anyString(),Mockito.any(PageRequest.class));
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNotNull(result.getBody()),
                () -> assertEquals(200,
                        result.getStatusCode()
                                .value()));
    }

    @Test
    void buildChart() {
        //given
        Mockito.when(repository.findTop53ByUserIdOrderByStatementDateDesc(Mockito.anyString()))
                .thenReturn(Collections.singletonList(WaterIndexStatement.builder()
                        .id(1L)
                        .build()));
        Mockito.when(chartService.waterConsumptionCalculator(Mockito.anyList()))
                .thenReturn(Collections.singletonList(ChartContainer.builder()
                        .name("chartContainer")
                        .build()));

        //when
        ResponseEntity result = controller.buildChart("userId");

        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .findTop53ByUserIdOrderByStatementDateDesc(Mockito.anyString());
        Mockito.verify(chartService,
                Mockito.times(1))
                .waterConsumptionCalculator(Mockito.anyList());
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNotNull(result.getBody()),
                () -> assertEquals(200,
                        result.getStatusCode()
                                .value()));
    }

    @Test
    void findOne() {
        //given
        Mockito.when(repository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(WaterIndexStatement.builder()
                        .id(1L)
                        .build()));
        //when
        ResponseEntity result = controller.findOne(1L);
        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .findById(Mockito.anyLong());
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNotNull(result.getBody()),
                () -> assertEquals(200,
                        result.getStatusCode()
                                .value()));
    }

    @Test
    void create() {
        //given
        Mockito.when(repository.save(Mockito.any(WaterIndexStatement.class)))
                .thenReturn(WaterIndexStatement.builder()
                        .id(1L)
                        .build());
        //when
        ResponseEntity result = controller.create(WaterIndexStatement.builder()
                .build());
        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .save(Mockito.any(WaterIndexStatement.class));
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNotNull(result.getBody()),
                () -> assertEquals(200,
                        result.getStatusCode()
                                .value()));
    }

    @Test
    void update() {
        //given
        Mockito.when(repository.save(Mockito.any(WaterIndexStatement.class)))
                .thenReturn(WaterIndexStatement.builder()
                        .id(1L)
                        .build());
        //when
        ResponseEntity result = controller.update(WaterIndexStatement.builder()
                .build());
        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .save(Mockito.any(WaterIndexStatement.class));
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNotNull(result.getBody()),
                () -> assertEquals(200,
                        result.getStatusCode()
                                .value()));
    }

    @Test
    void delete() {
        //given
        Mockito.doNothing().when(repository).deleteById(Mockito.anyLong());
        //when
        ResponseEntity result = controller.delete(1L);
        //then
        Mockito.verify(repository,
                Mockito.times(1))
                .deleteById(Mockito.anyLong());
        assertAll("response",
                () -> assertNotNull(result),
                () -> assertNull(result.getBody()),
                () -> assertEquals(204,
                        result.getStatusCode()
                                .value()));
    }
    @Test
    @DisplayName("should call deleteInBatch and return http 204")
    void wipeTest10() {
        //given

        //when
        ResponseEntity result = controller.wipe("userId");
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .findByUserIdOrderByStatementDateDesc(Mockito.anyString());
        Mockito.verify(repository,
                       Mockito.times(1))
                .deleteInBatch(Mockito.anyIterable());
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(204,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should throw an IOException en return http 500")
    void exportCsvTest10() throws Exception {
        //given
        Mockito.doThrow(IOException.class)
                .when(csvFileMapper)
                .write(Mockito.eq(WaterIndexStatementCsv.class),
                       Mockito.anyList());
        //when
        ResponseEntity result = controller.exportCsv("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(500,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 200 with a body")
    void exportCsvTest20() throws Exception {
        //given
        Mockito.when(csvFileMapper.write(Mockito.eq(WaterIndexStatementCsv.class),
                                         Mockito.anyList()))
                .thenReturn("csv;file".getBytes());
        LocalDate now = LocalDate.now();
        Mockito.when(repository.findByUserIdOrderByStatementDateDesc(Mockito.anyString()))
                .thenReturn(Collections.singletonList(WaterIndexStatement.builder()
                                                              .statementDate(now)
                                                              .build()));
        Mockito.when(mapper.toCsv(Mockito.anyList()))
                .thenReturn(Collections.singletonList(WaterIndexStatementCsv.builder()
                                                              .statementDate(now)
                                                              .build()));
        //when
        ResponseEntity result = controller.exportCsv("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200,
                                     result.getStatusCodeValue()),
                  () -> assertNotNull(result.getBody()));
    }

    @Test
    @DisplayName("should throw an IOException en return http 500")
    void importMultipartTest10() throws Exception {
        //given
        Mockito.doThrow(IOException.class)
                .when(csvFileMapper)
                .read(Mockito.eq(WaterIndexStatementCsv.class),
                      Mockito.any(InputStream.class));
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "csv;file".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(500,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 411 if file is empty")
    void importMultipartTest20() throws Exception {
        //given
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(411,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 200 with a body")
    void importMultipartTest30() throws Exception {
        //given
        Mockito.when(csvFileMapper.read(Mockito.eq(WaterIndexStatementCsv.class),
                                        Mockito.any(InputStream.class)))
                .thenReturn(Collections.singletonList(WaterIndexStatementCsv.builder()
                                                              .statementDate(LocalDate.now())
                                                              .build()));
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "csv;file".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200,
                                     result.getStatusCodeValue()),
                  () -> assertNotNull(result.getBody())
        );
    }
}