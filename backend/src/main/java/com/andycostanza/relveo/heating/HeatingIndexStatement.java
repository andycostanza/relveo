package com.andycostanza.relveo.heating;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by andy on 24/03/17.
 */
@Entity
@Table(indexes = {@Index(columnList = "userId", name = "heating_userid_idx")})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HeatingIndexStatement implements Serializable,Comparable<HeatingIndexStatement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /*@Temporal(TemporalType.DATE)*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate statementDate;
    private BigDecimal heightOfTank;
    private String userId;

    @Override
    public int compareTo(HeatingIndexStatement h) {
        return getStatementDate().compareTo(h.getStatementDate());
    }
}
