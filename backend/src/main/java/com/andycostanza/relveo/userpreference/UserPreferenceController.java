package com.andycostanza.relveo.userpreference;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@RestController
@RequestMapping("/api/user-preference")
@RequiredArgsConstructor
public class UserPreferenceController {
    private final UserPreferenceRepository repository;
    @PostMapping("/{id}")
    public ResponseEntity upsertUserPreference(@PathVariable("id") String userId) {
        Optional<UserPreference> optionalUserPreference = repository.findById(userId);
        if(optionalUserPreference.isPresent()){
            UserPreference userPreference = optionalUserPreference.get();
            userPreference
                    .setLastLogin(LocalDate.now());
            userPreference
                    .setLastLoginTime(LocalTime.now());
            return ResponseEntity.ok(repository.save(userPreference));
        }else{
            UserPreference userPreference = UserPreference.builder()
                    .id(userId)
                    .lastLogin(LocalDate.now())
                    .lastLoginTime(LocalTime.now())
                    .tankType(TankType.CUBOID)
                    .lengthOfTank(BigDecimal.valueOf(225D))
                    .widthOfTank(BigDecimal.valueOf(135D))
                    .build();
            return ResponseEntity.ok(repository.save(userPreference));
        }
    }

    @PutMapping()
    public ResponseEntity update(@RequestBody UserPreference userPreference) {
        return ResponseEntity.ok(repository.save(userPreference));
    }

}
