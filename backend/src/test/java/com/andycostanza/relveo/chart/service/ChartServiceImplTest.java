package com.andycostanza.relveo.chart.service;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.ChartValue;
import com.andycostanza.relveo.electricity.ElectricityIndexStatement;
import com.andycostanza.relveo.heating.HeatingIndexStatement;
import com.andycostanza.relveo.userpreference.TankType;
import com.andycostanza.relveo.userpreference.UserPreference;
import com.andycostanza.relveo.water.WaterIndexStatement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("ChartService implementation")
class ChartServiceImplTest {
    ChartService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new ChartServiceImpl();
    }

    @Test
    @DisplayName("should return a empty chartContainers when ElectricityIndexStatement list is null")
    void electricityConsumptionCalculatorTest10() {
        //when
        List<ChartContainer> result = service.electricityConsumptionCalculator(null);
        //then
        assertEquals(Collections.EMPTY_LIST,
                     result);
    }

    @Test
    void electricityConsumptionCalculatorTest20() {
        //given
        //when
        LocalDate now = LocalDate.now();
        LocalDate today = now.atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        ElectricityIndexStatement statement1 = ElectricityIndexStatement.builder()
                .statementDate(today)
                .dayIndex(BigDecimal.valueOf(15D))
                .nightIndex(BigDecimal.valueOf(5D))
                .build();
        LocalDate todayPlus1Day = now.plus(1,
                                           ChronoUnit.DAYS)
                .atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        ElectricityIndexStatement statement2 = ElectricityIndexStatement.builder()
                .statementDate(todayPlus1Day)
                .dayIndex(BigDecimal.valueOf(20D))
                .nightIndex(BigDecimal.valueOf(15D))
                .build();
        List<ChartContainer> result = service.electricityConsumptionCalculator(Arrays.asList(statement1,
                                                                                             statement2));
        //then
        assertAll("chart container list",
                  () -> assertNotNull(result),
                  () -> result.forEach(container -> {
                      org.assertj.core.api.Assertions.assertThat(container.getSeries())
                              .containsAnyOf(ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.valueOf(5D))
                                                     .build(),
                                             ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.valueOf(10D))
                                                     .build(),
                                             ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.valueOf(15D))
                                                     .build());
                  }),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(0)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.valueOf(5D))
                                            .build()),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(1)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.valueOf(10D))
                                            .build()));
    }

    @Test
    @DisplayName("should return a empty chartContainers when HeatingIndexStatement list and userPreference are null")
    void heatingConsumptionCalculatorTest10() {
        //when
        List<ChartContainer> result = service.heatingConsumptionCalculator(null,
                                                                           null);
        //then
        assertEquals(Collections.EMPTY_LIST,
                     result);
    }

    @Test
    @DisplayName("eturn a empty chartContainers when userPreference is null")
    void heatingConsumptionCalculatorTest11() {
        //when
        List<ChartContainer> result =
                service.heatingConsumptionCalculator(Collections.singletonList(HeatingIndexStatement.builder()
                                                                                       .build()),
                                                     null);
        //then
        assertEquals(Collections.EMPTY_LIST,
                     result);
    }

    @Test
    @DisplayName("should throw IllegalArgumentException if user preference is empty")
    void heatingConsumptionCalculatorTest12() {
        assertThrows(IllegalArgumentException.class,
                     () -> service.heatingConsumptionCalculator(Collections.singletonList(HeatingIndexStatement.builder()
                                                                                                  .build()),
                                                                UserPreference.builder()
                                                                        .build()));
    }

    @Test
    @DisplayName("should throw IllegalArgumentException if the widthOfTank is null in the userPreference")
    void heatingConsumptionCalculatorTest13() {
        assertThrows(IllegalArgumentException.class,
                     () -> service.heatingConsumptionCalculator(Collections.singletonList(HeatingIndexStatement.builder()
                                                                                                  .build()),
                                                                UserPreference.builder()
                                                                        .lengthOfTank(BigDecimal.ONE)
                                                                        .tankType(TankType.CUBOID)
                                                                        .build()));
    }
    @Test
    @DisplayName("should throw IllegalArgumentException if the lengthOfTank is null in the userPreference")
    void heatingConsumptionCalculatorTest14() {
        assertThrows(IllegalArgumentException.class,
                     () -> service.heatingConsumptionCalculator(Collections.singletonList(HeatingIndexStatement.builder()
                                                                                                  .build()),
                                                                UserPreference.builder()
                                                                        .widthOfTank(BigDecimal.ONE)
                                                                        .tankType(TankType.CUBOID)
                                                                        .build()));
    }
    @Test
    @DisplayName("should throw IllegalArgumentException if the tankType is null in the userPreference")
    void heatingConsumptionCalculatorTest15() {
        assertThrows(IllegalArgumentException.class,
                     () -> service.heatingConsumptionCalculator(Collections.singletonList(HeatingIndexStatement.builder()
                                                                                                  .build()),
                                                                UserPreference.builder()
                                                                        .widthOfTank(BigDecimal.ONE)
                                                                        .lengthOfTank(BigDecimal.ONE)
                                                                        .build()));
    }

    @Test
    @DisplayName("should return a consumption at 0 in chartContainers if we refill the tank")
    void heatingConsumptionCalculatorTest20() {
        //given
        //when
        LocalDate now = LocalDate.now();
        LocalDate today = now.atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        HeatingIndexStatement statement1 = HeatingIndexStatement.builder()
                .statementDate(today)
                .heightOfTank(BigDecimal.valueOf(5D))
                .build();
        LocalDate todayPlus1Day = now.plus(1,
                                           ChronoUnit.DAYS)
                .atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        HeatingIndexStatement statement2 = HeatingIndexStatement.builder()
                .statementDate(todayPlus1Day)
                .heightOfTank(BigDecimal.valueOf(20D)) // <-- refill the tank
                .build();
        List<ChartContainer> result = service.heatingConsumptionCalculator(Arrays.asList(statement1,
                                                                                         statement2),
                                                                           UserPreference.builder()
                                                                                   .lengthOfTank(BigDecimal.valueOf(100L))
                                                                                   .widthOfTank(BigDecimal.valueOf(10L))
                                                                                   .tankType(TankType.CUBOID)
                                                                                   .build());
        //then
        assertAll("chart container list",
                  () -> assertNotNull(result),
                  () -> result.forEach(container -> {
                      org.assertj.core.api.Assertions.assertThat(container.getSeries())
                              .containsAnyOf(ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.ZERO)
                                                     .build());
                  }),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(0)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.ZERO)
                                            .build())
        );
    }

    @Test
    @DisplayName("should return a consumption at 0 in chartContainers if we refill the tank")
    void heatingConsumptionCalculatorTest30() {
        //given
        //when
        LocalDate now = LocalDate.now();
        LocalDate today = now.atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        HeatingIndexStatement statement1 = HeatingIndexStatement.builder()
                .statementDate(today)
                .heightOfTank(BigDecimal.valueOf(20D))
                .build();
        LocalDate todayPlus1Day = now.plus(1,
                                           ChronoUnit.DAYS)
                .atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        HeatingIndexStatement statement2 = HeatingIndexStatement.builder()
                .statementDate(todayPlus1Day)
                .heightOfTank(BigDecimal.valueOf(15D))
                .build();
        List<ChartContainer> result = service.heatingConsumptionCalculator(Arrays.asList(statement1,
                                                                                         statement2),
                                                                           UserPreference.builder()
                                                                                   .lengthOfTank(BigDecimal.valueOf(100L))
                                                                                   .widthOfTank(BigDecimal.valueOf(10L))
                                                                                   .tankType(TankType.CUBOID)
                                                                                   .build());
        //then
        assertAll("chart container list",
                  () -> assertNotNull(result),
                  () -> result.forEach(container -> {
                      org.assertj.core.api.Assertions.assertThat(container.getSeries())
                              .containsAnyOf(ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.valueOf(5D))
                                                     .build());
                  }),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(0)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.valueOf(5D))
                                            .build())
        );
    }

    @Test
    @DisplayName("should return a empty chartContainers when WaterIndexStatement list is null")
    void waterConsumptionCalculatorTest10() {
        //when
        List<ChartContainer> result = service.waterConsumptionCalculator(null);
        //then
        assertEquals(Collections.EMPTY_LIST,
                     result);
    }
    @Test
    @DisplayName("should return a consumption at 0 in chartContainers if we reset the water counter")
    void waterConsumptionCalculatorTest20() {
        //given
        //when
        LocalDate now = LocalDate.now();
        LocalDate today = now.atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        WaterIndexStatement statement1 = WaterIndexStatement.builder()
                .statementDate(today)
                .waterIndex(BigDecimal.valueOf(500D))
                .build();
        LocalDate todayPlus1Day = now.plus(1,
                                           ChronoUnit.DAYS)
                .atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        WaterIndexStatement statement2 = WaterIndexStatement.builder()
                .statementDate(todayPlus1Day)
                .waterIndex(BigDecimal.valueOf(0D)) // <-- reset the water counter
                .build();
        List<ChartContainer> result = service.waterConsumptionCalculator(Arrays.asList(statement1,
                                                                                         statement2));
        //then
        assertAll("chart container list",
                  () -> assertNotNull(result),
                  () -> result.forEach(container -> {
                      org.assertj.core.api.Assertions.assertThat(container.getSeries())
                              .containsAnyOf(ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.ZERO)
                                                     .build());
                  }),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(0)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.ZERO)
                                            .build())
        );
    }

    @Test
    @DisplayName("should return a consumption at 0 in chartContainers if we refill the tank")
    void waterConsumptionCalculatorTest30() {
        //given
        //when
        LocalDate now = LocalDate.now();
        LocalDate today = now.atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        WaterIndexStatement statement1 = WaterIndexStatement.builder()
                .statementDate(today)
                .waterIndex(BigDecimal.valueOf(15D))
                .build();
        LocalDate todayPlus1Day = now.plus(1,
                                           ChronoUnit.DAYS)
                .atStartOfDay(ZoneId.systemDefault())
                .toLocalDate();
        WaterIndexStatement statement2 = WaterIndexStatement.builder()
                .statementDate(todayPlus1Day)
                .waterIndex(BigDecimal.valueOf(20D))
                .build();
        List<ChartContainer> result = service.waterConsumptionCalculator(Arrays.asList(statement1,
                                                                                         statement2));
        //then
        assertAll("chart container list",
                  () -> assertNotNull(result),
                  () -> result.forEach(container -> {
                      org.assertj.core.api.Assertions.assertThat(container.getSeries())
                              .containsAnyOf(ChartValue.builder()
                                                     .name(todayPlus1Day.toString())
                                                     .value(BigDecimal.valueOf(5D))
                                                     .build());
                  }),
                  () -> org.assertj.core.api.Assertions.assertThat(result.get(0)
                                                                           .getSeries())
                          .contains(ChartValue.builder()
                                            .name(todayPlus1Day.toString())
                                            .value(BigDecimal.valueOf(5D))
                                            .build())
        );
    }
}