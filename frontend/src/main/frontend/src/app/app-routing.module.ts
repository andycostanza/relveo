/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ElectricityComponent} from "./electricity/electricity.component";
import {HeatingComponent} from "./heating/heating.component";
import {WaterComponent} from "./water/water.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ElectricityDetailComponent} from "./electricity-detail/electricity-detail.component";
import {HeatingDetailComponent} from "./heating-detail/heating-detail.component";
import {WaterDetailComponent} from "./water-detail/water-detail.component";
import {ProfileComponent} from "./profile/profile.component";
import {AuthGuard} from "./auth.guard";
import {AboutComponent} from "./about/about.component";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {InterceptorService} from "./interceptor.service";

const routes: Routes = [
  { path: '', component: AboutComponent , pathMatch: 'full' },
/*  { path: 'about', component: AboutComponent  },*/
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'electricity', component: ElectricityComponent, canActivate: [AuthGuard]  },
  { path: 'electricity/:id', component: ElectricityDetailComponent, canActivate: [AuthGuard]  },
  { path: 'heating/:id', component: HeatingDetailComponent, canActivate: [AuthGuard]  },
  { path: 'water/:id', component: WaterDetailComponent , canActivate: [AuthGuard] },
  { path: 'heating', component: HeatingComponent, canActivate: [AuthGuard]  },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'water', component: WaterComponent , canActivate: [AuthGuard] }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ]
})
export class AppRoutingModule { }
