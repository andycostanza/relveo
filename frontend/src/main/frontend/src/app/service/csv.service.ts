/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


import {Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const httpOptionsForDownload = {
  responseType: 'blob' as 'json',
  headers: new HttpHeaders({'Content-Type': 'text/csv'})
};

@Injectable({
  providedIn: 'root'
})
export class CsvService {

  constructor(private httpClient: HttpClient) {
  }

  public import(url, userId, formData) {
    return this.httpClient.post<any>(url + '/import?userId=' + userId, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }
  public export(url, userId): Observable<any>{
    return this.httpClient.get<Blob>(url + '/export?userId='+userId, httpOptionsForDownload);
  }

  public deleteAll(url, userId) {
    return this.httpClient.delete<any>(url + '/wipe?userId=' + userId, httpOptions);
  }

}
