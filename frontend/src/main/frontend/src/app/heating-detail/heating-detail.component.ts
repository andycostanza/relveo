/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {HeatingService} from '../service/heating.service';

@Component({
  selector: 'app-heating-detail',
  templateUrl: './heating-detail.component.html',
  styleUrls: ['./heating-detail.component.css']
})
export class HeatingDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              public heatingService: HeatingService,
              private location: Location) { }

  ngOnInit() {
    this.getRecord();
  }

  getRecord() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heatingService.getOne(id);
  }
  goBack(): void {
    this.location.back();
  }
  onSubmit(): void {
    console.log("PUT heating to backend : " /*+ JSON.stringify(this.heating)*/);
    this.heatingService.update(this.heatingService.heating);
    this.goBack()
  }
}
