-- first run mariadb: $> mariadbd --console
CREATE DATABASE IF NOT EXISTS relveo;
CREATE USER IF NOT EXISTS 'relveo'@localhost IDENTIFIED BY 'password1';
GRANT ALL PRIVILEGES ON relveo.* TO 'relveo'@localhost;
FLUSH PRIVILEGES;