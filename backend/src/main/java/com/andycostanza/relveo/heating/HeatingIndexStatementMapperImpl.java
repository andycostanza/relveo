package com.andycostanza.relveo.heating;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HeatingIndexStatementMapperImpl implements HeatingIndexStatementMapper {
    private final HeatingIndexStatementRepository repository;
    @Override
    public List<HeatingIndexStatement> toEntities(List<HeatingIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(heatingIndexStatementCsv -> {
                    Optional<HeatingIndexStatement> entity = repository.findByUserIdAndStatementDate(userId, heatingIndexStatementCsv.getStatementDate());
                    if (entity.isPresent()){
                        entity.get().setHeightOfTank(heatingIndexStatementCsv.getHeightOfTank());
                        return entity.get();
                    }else{
                        return HeatingIndexStatement.builder()
                                .statementDate(heatingIndexStatementCsv.getStatementDate())
                                .heightOfTank(heatingIndexStatementCsv.getHeightOfTank())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<HeatingIndexStatementCsv> toCsv(List<HeatingIndexStatement> entities) {
        return entities.stream()
                .map(heatingIndexStatement -> HeatingIndexStatementCsv.builder()
                        .statementDate(heatingIndexStatement.getStatementDate())
                        .heightOfTank(heatingIndexStatement.getHeightOfTank())
                        .build())
                .collect(Collectors.toList());
    }
}
