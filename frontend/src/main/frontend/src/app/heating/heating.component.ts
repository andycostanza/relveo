/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Component, OnInit} from '@angular/core';
import {HeatingService} from '../service/heating.service';
import {Heating} from '../model/heating';
import {AuthService} from '../auth.service';
import {UserPreferenceService} from "../service/user-preference.service";


@Component({
  selector: 'app-heating',
  templateUrl: './heating.component.html',
  styleUrls: ['./heating.component.css']
})
export class HeatingComponent implements OnInit {
  litterByCm = 0;
  model = new Heating;
  url: any;

  constructor(public heatingService: HeatingService, public userPreferenceService: UserPreferenceService, public auth: AuthService) {

  }

  ngOnInit() {
    this.litterByCm = (this.userPreferenceService.userPreference.lengthOfTank * this.userPreferenceService.userPreference.widthOfTank)/1000
    this.loadData();
    this.url = this.heatingService.getUrl();
  }
  refresh(newItem: string) {
    this.loadData();
    this.refreshChart();
  }

  onSubmit() {
    this.model.userId = this.auth.getUserId();
    console.log("POST heating to backend : " /*+ JSON.stringify(this.model)*/);
    this.heatingService.add(this.model);
    this.refreshChart();
  }

  delete(heating: Heating): void {
    this.heatingService.delete(heating);
    this.refreshChart();
  }

  refreshChart() {
    this.heatingService.getChart(this.auth.getUserId());
  }

  loadPage(page: number) {
    if (page !== this.heatingService.previousPage) {
      this.heatingService.previousPage = page;
      this.loadData();
    }
  }

  loadData() {
    if(this.heatingService.heatingList.length == 0) {
      this.heatingService.getAllPaginate(this.auth.getUserId(),
        this.heatingService.page - 1,
        this.heatingService.itemsPerPage,
      );
    }
  }
}
