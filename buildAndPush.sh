#!/bin/bash
mvn -U clean verify -Pdocker -Pmariadb -Pembed -DskipTests -f pom.xml
docker buildx build -t costalfy/relveo --platform=linux/arm,linux/arm64,linux/amd64 . --push