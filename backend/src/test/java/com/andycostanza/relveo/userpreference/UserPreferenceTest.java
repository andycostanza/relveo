package com.andycostanza.relveo.userpreference;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserPreferenceTest {

    @Test
    @DisplayName("should throw an IllegalArgumentException if width and length are null")
    void surfaceOfTankTest10() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .build();
        //when

        //then
        assertThrows(IllegalArgumentException.class,
                     () -> userPreference.getSurfaceOfTank());
    }

    @Test
    @DisplayName("should throw an IllegalArgumentException if width is null")
    void surfaceOfTankTest20() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .lengthOfTank(BigDecimal.ONE)
                .build();
        //when

        //then
        assertThrows(IllegalArgumentException.class,
                     () -> userPreference.getSurfaceOfTank());
    }

    @Test
    @DisplayName("should throw an IllegalArgumentException if width is null")
    void surfaceOfTankTest30() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .widthOfTank(BigDecimal.ONE)
                .build();
        //when

        //then
        assertThrows(IllegalArgumentException.class,
                     () -> userPreference.getSurfaceOfTank());
    }

    @Test
    @DisplayName("should throw an IllegalArgumentException if typeOfTank is null")
    void surfaceOfTankTest40() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .widthOfTank(BigDecimal.valueOf(10.2D))
                .lengthOfTank(BigDecimal.valueOf(10.3D))
                .build();
        //when
        //then
        assertThrows(IllegalArgumentException.class,
                     () -> userPreference.getSurfaceOfTank());
    }

    @Test
    @DisplayName("should return an Optional of 105.06 if width equals 10.2 and length equal 10.3 for a cuboid")
    void surfaceOfTankTest50() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .widthOfTank(BigDecimal.valueOf(10.2D))
                .lengthOfTank(BigDecimal.valueOf(10.3D))
                .tankType(TankType.CUBOID)
                .build();
        //when
        Optional<BigDecimal> result = userPreference.getSurfaceOfTank();
        //then
        assertAll("result",
                  () -> assertTrue(result.isPresent()),
                  () -> assertEquals(Optional.of(BigDecimal.valueOf(105.06D)),
                                     result));
    }

    @Test
    @DisplayName("should return Optional empty for a type of tank other than cuboid")
    void surfaceOfTankTest60() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .widthOfTank(BigDecimal.valueOf(10.2D))
                .lengthOfTank(BigDecimal.valueOf(10.3D))
                .tankType(TankType.HORIZONTAL_CYLINDER)
                .build();
        //when
        Optional<BigDecimal> result = userPreference.getSurfaceOfTank();
        //then
        assertAll("result",
                  () -> assertEquals(Optional.empty(),
                                     result));
    }
}