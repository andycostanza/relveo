/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UserPreference} from "../model/user-preference";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserPreferenceService {

  private url = '/api/user-preference/';
  userPreference: UserPreference;

  constructor(private http: HttpClient) {
  }

  saveOrUpdate(userId: string) {
    return this.http.post<UserPreference>(this.url + userId, '', httpOptions)
      .subscribe(userPreference => this.userPreference = userPreference);
  }

  /** PUT: update the user preference on the server */
  update(userPreference: UserPreference) {
    return this.http.put<UserPreference>(this.url, userPreference, httpOptions)
      .subscribe(userPreference => this.userPreference = userPreference);
  }
}
