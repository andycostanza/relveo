package com.andycostanza.relveo.electricity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by andy on 24/03/17.
 */
@Repository
public interface ElectricityIndexStatementRepository extends JpaRepository<ElectricityIndexStatement, Long> {
    List<ElectricityIndexStatement> findTop53ByUserIdOrderByStatementDateDesc(String userId);

    Page<ElectricityIndexStatement> findByUserId(String userId, Pageable page);

    List<ElectricityIndexStatement> findByUserIdOrderByStatementDateDesc(String userId);

    Optional<ElectricityIndexStatement> findByUserIdAndStatementDate(String userId, LocalDate statementDate);
}
