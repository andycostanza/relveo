package com.andycostanza.relveo.heating;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class HeatingIndexStatementMapperImplTest {

    HeatingIndexStatementMapper mapper;
    @Mock
    HeatingIndexStatementRepository repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new HeatingIndexStatementMapperImpl(repository);
    }

    @Test
    @DisplayName("should return new HeatingIndexStatement if it doesn't find in the repository")
    void toEntitiesTest10() {
        //given
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.empty());
        //when
        LocalDate now = LocalDate.now();
        HeatingIndexStatementCsv csv = HeatingIndexStatementCsv.builder()
                .statementDate(now)
                .heightOfTank(BigDecimal.ONE)
                .build();
        List<HeatingIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertNull(result.get(0)
                                           .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getHeightOfTank()),

                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return an updated existing HeatingIndexStatement if it find in the repository")
    void toEntitiesTest20() {
        //given
        LocalDate now = LocalDate.now();
        HeatingIndexStatement heatingIndexStatement = HeatingIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .heightOfTank(BigDecimal.ZERO)
                .statementDate(now)
                .build();
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.of(heatingIndexStatement));
        //when

        HeatingIndexStatementCsv csv = HeatingIndexStatementCsv.builder()
                .statementDate(now)
                .heightOfTank(BigDecimal.ONE)
                .build();
        List<HeatingIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(1L,
                                     result.get(0)
                                             .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getHeightOfTank()),
                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return a List of HeatingIndexStatementCsv")
    void toCsvTest10() {
        //given
        LocalDate now = LocalDate.now();
        HeatingIndexStatement heatingIndexStatement = HeatingIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .heightOfTank(BigDecimal.ONE)
                .statementDate(now)
                .build();
        //when
        List<HeatingIndexStatementCsv> result = mapper.toCsv(Collections.singletonList(heatingIndexStatement));
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getHeightOfTank()));
    }
}