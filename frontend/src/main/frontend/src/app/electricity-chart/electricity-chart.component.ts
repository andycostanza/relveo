/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Component, OnInit} from '@angular/core';
import {ElectricityService} from "../service/electricity.service";
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-electricity-chart',
  templateUrl: './electricity-chart.component.html',
  styleUrls: ['./electricity-chart.component.css']
})
export class ElectricityChartComponent implements OnInit {


  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Electricity';
  showYAxisLabel = true;
  yAxisLabel = 'kW/h';

  colorScheme = {
    domain: ['#0000FF', '#000000', '#AA0000']
  };

  // line, area
  autoScale = true;
  constructor(public electricityService: ElectricityService, public auth: AuthService) {

  }

  ngOnInit() {
    this.initElectricityChart();
  }
  onSelect(event) {
    console.log(event);
  }

  initElectricityChart() {
    if(this.electricityService.multi.length == 0) {
      this.electricityService.getChart(this.auth.getUserId());
    }
  }

}
