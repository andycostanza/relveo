package com.andycostanza.relveo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

public interface CsvFileMapper extends Serializable {
    <T> List<T> read(Class<T> clazz, InputStream stream) throws IOException;

    <T> byte[] write(Class<T> clazz, List<T> list) throws IOException;
}
