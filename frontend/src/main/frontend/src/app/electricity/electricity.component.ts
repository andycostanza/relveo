/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Component, OnInit} from '@angular/core';
import {ElectricityService} from '../service/electricity.service';
import {Electricity} from '../model/electricity';
import {AuthService} from "../auth.service";


@Component({
  selector: 'app-electricity',
  templateUrl: './electricity.component.html',
  styleUrls: ['./electricity.component.css']
})
export class ElectricityComponent implements OnInit {
  model = new Electricity;
  url: any;

  constructor(public electricityService: ElectricityService, public auth: AuthService) {
  }

  ngOnInit() {
    this.loadData();
    this.url = this.electricityService.getUrl();
  }

  refresh(newItem: string) {
    this.loadData();
    this.refreshChart();
  }

  onSubmit() {
    this.model.userId = this.auth.getUserId();
    this.electricityService.add(this.model);
    this.refreshChart();
  }

  delete(electricity: Electricity): void {
    this.electricityService.delete(electricity);
    this.refreshChart();
  }

  refreshChart() {
    this.electricityService.getChart(this.auth.getUserId());
  }

  loadPage(page: number) {
    if (page !== this.electricityService.previousPage) {
      this.electricityService.previousPage = page;
      this.loadData();
    }
  }

  loadData() {
    if(this.electricityService.electricityList.length == 0) {
      this.electricityService.getAllPaginate(this.auth.getUserId(),
        this.electricityService.page - 1,
        this.electricityService.itemsPerPage,
      );
    }
  }


}
