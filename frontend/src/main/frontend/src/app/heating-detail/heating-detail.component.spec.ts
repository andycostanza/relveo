import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HeatingDetailComponent} from './heating-detail.component';

describe('HeatingDetailComponent', () => {
  let component: HeatingDetailComponent;
  let fixture: ComponentFixture<HeatingDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
