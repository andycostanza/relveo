/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Water} from "../model/water";
import {Page} from "../model/page";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class WaterService {
  private url = '/api/waterIndexStatements';
  multi: any[] = [];

  waterList: Water[] = [];
  water: Water = new Water();
  itemsPerPage: number=10;
  totalItems: any;
  page: any=1;
  previousPage: any;
  constructor(private http: HttpClient) {
  }

  getUrl(): string {
    return this.url
  }

  getChart(userId: string) {
    return this.http.get<any>(this.url + '/chart?userId=' + userId)
      .subscribe(list => this.multi = list);
  }

  getAllPaginate(userId: string, page: number, size: number){
    return this.http.get<Page>(this.url + '?userId=' + userId + '&size=' + size + '&page=' + page)
      .subscribe(
        response => {
          this.waterList = response.content;
          this.totalItems = response.totalElements;
          this.itemsPerPage = response.size;
        });
  }

  getOne(id: number){
    const url = `${this.url}/${id}`;
    return this.http.get<Water>(url)
      .subscribe(statementFounded => this.water = statementFounded);
  }

  //////// Save methods //////////

  /** POST: add a new waterForm to the server */
  add(water: Water){
    return this.http.post<Water>(this.url, water, httpOptions)
      .subscribe(statementSaved => this.waterList.unshift(statementSaved));
  }

  /** DELETE: delete the water from the server */
  delete(water: Water | number) {
    const id = typeof water === 'number' ? water : water.id;
    const url = `${this.url}/${id}`;

    return this.http.delete<Water>(url, httpOptions)
      .subscribe(() => this.waterList = this.waterList.filter(e => e !== water));
  }

  /** PUT: update the water on the server */
  update(water: Water){
    return this.http.put<Water>(this.url, water, httpOptions)
      .subscribe(statementUpdated => {
      let itemIndex = this.waterList.findIndex(item => item.id == water.id);
      this.waterList[itemIndex] = statementUpdated;
    });
  }
}
