/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {ElectricityComponent} from './electricity/electricity.component';
import {HeatingComponent} from './heating/heating.component';
import {WaterComponent} from './water/water.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ElectricityService} from './service/electricity.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ElectricityDetailComponent} from './electricity-detail/electricity-detail.component';
import {ElectricityChartComponent} from './electricity-chart/electricity-chart.component';
import {WaterChartComponent} from './water-chart/water-chart.component';
import {HeatingChartComponent} from './heating-chart/heating-chart.component';
import {WaterDetailComponent} from './water-detail/water-detail.component';
import {HeatingDetailComponent} from './heating-detail/heating-detail.component';
import {WaterService} from './service/water.service';
import {HeatingService} from './service/heating.service';
import {ProfileComponent} from './profile/profile.component';
import {NavbarComponent} from './navbar/navbar.component';
import {AboutComponent} from './about/about.component';
import {CsvComponent} from './csv/csv.component';
import {HttpErrorInterceptor} from "./http-error.interceptor";
import {GlobalErrorHandler} from "./global-error.handler";
import {NotificationComponent} from './notification/notification.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    ElectricityComponent,
    HeatingComponent,
    WaterComponent,
    DashboardComponent,
    ElectricityDetailComponent,
    ElectricityChartComponent,
    WaterChartComponent,
    HeatingChartComponent,
    WaterDetailComponent,
    HeatingDetailComponent,
    ProfileComponent,
    NavbarComponent,
    AboutComponent,
    CsvComponent,
    NotificationComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxChartsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true
  },
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    ElectricityService, WaterService, HeatingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
