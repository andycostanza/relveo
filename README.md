# Relveo

![build status](https://gitlab.com/andycostanza/relveo/badges/master/pipeline.svg)
![test coverage](https://gitlab.com/andycostanza/relveo/badges/master/coverage.svg)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=andycostanza_relveo&metric=alert_status)](https://sonarcloud.io/dashboard?id=andycostanza_relveo)

Relveo is a simple [Spring Boot](https://projects.spring.io/spring-boot/) backend with an [Angular](https://angular.io) frontend embedded

