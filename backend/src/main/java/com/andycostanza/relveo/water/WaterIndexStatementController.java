package com.andycostanza.relveo.water;

import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.utils.CsvFileMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/waterIndexStatements")
@ExposesResourceFor(WaterIndexStatement.class)
@Slf4j
public class WaterIndexStatementController {
    private final WaterIndexStatementRepository repository;
    private final ChartService chartService;
    private final CsvFileMapper csvFileMapper;
    private final WaterIndexStatementMapper mapper;

    @PostMapping(value = "/import", consumes = "multipart/form-data")
    public ResponseEntity importMultipart(@RequestParam("userId") String userId,
                                          @RequestParam("file") MultipartFile file) {
        try {
            if (file.getSize() > 0) {
                List<WaterIndexStatementCsv> csvDatas = csvFileMapper.read(WaterIndexStatementCsv.class,
                                                                           file.getInputStream());
                List<WaterIndexStatement> entities = mapper.toEntities(csvDatas,
                                                                       userId);
                return ResponseEntity.ok(repository.saveAll(entities));
            } else {
                return ResponseEntity.status(HttpStatus.LENGTH_REQUIRED)
                        .body("The CSV file is empty");
            }
        } catch (IOException e) {
            log.error(e.getMessage(),
                      e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    @GetMapping("/export")
    public ResponseEntity exportCsv(@RequestParam("userId") String userId) {
        try {
            List<WaterIndexStatement> entities = repository.findByUserIdOrderByStatementDateDesc(userId);
            List<WaterIndexStatementCsv> csv = mapper.toCsv(entities);
            byte[] bytes = csvFileMapper.write(WaterIndexStatementCsv.class,
                                               csv);

            ByteArrayResource resource = new ByteArrayResource(bytes);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=data.csv");
            headers.add("Cache-Control",
                        "no-cache, no-store, must-revalidate");
            headers.add("Pragma",
                        "no-cache");
            headers.add("Expires",
                        "0");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(resource.contentLength())
                    .contentType(MediaType.parseMediaType("text/csv"))
                    .body(resource);
        } catch (IOException e) {
            log.error(e.getMessage(),
                      e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    @DeleteMapping("/wipe")
    public ResponseEntity wipe(@RequestParam("userId") String userId) {
        repository.deleteInBatch(repository.findByUserIdOrderByStatementDateDesc(userId));
        return ResponseEntity.noContent()
                .build();
    }

    @GetMapping
    public ResponseEntity findAllPaginate(@RequestParam("userId") String userId,
                                          @RequestParam(name = "page", required = false) int page,
                                          @RequestParam(name = "size", required = false) int size) {
        return ResponseEntity.ok(repository.findByUserId(userId,
                                                         PageRequest.of(page,
                                                                        size,
                                                                        Sort.by(Sort.Direction.DESC,
                                                                                "statementDate"))));
    }

    @GetMapping("/chart")
    public ResponseEntity buildChart(@RequestParam("userId") String userId) {
        return ResponseEntity.ok(chartService.waterConsumptionCalculator(
                repository.findTop53ByUserIdOrderByStatementDateDesc(userId)));
    }

    @GetMapping("/{id}")
    public ResponseEntity findOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(repository.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody WaterIndexStatement waterIndexStatement) {
        return ResponseEntity.ok(repository.save(waterIndexStatement));
    }

    @PutMapping()
    public ResponseEntity update(@RequestBody WaterIndexStatement waterIndexStatement) {
        return ResponseEntity.ok(repository.save(waterIndexStatement));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        repository.deleteById(id);
        return ResponseEntity.noContent()
                .build();
    }
}
