package com.andycostanza.relveo.chart.service;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.electricity.ElectricityIndexStatement;
import com.andycostanza.relveo.heating.HeatingIndexStatement;
import com.andycostanza.relveo.userpreference.UserPreference;
import com.andycostanza.relveo.water.WaterIndexStatement;

import java.util.List;

public interface ChartService {
    List<ChartContainer> electricityConsumptionCalculator(List<ElectricityIndexStatement> indexStatements);

    List<ChartContainer> heatingConsumptionCalculator(List<HeatingIndexStatement> indexStatements, UserPreference userPreference) throws IllegalArgumentException;

    List<ChartContainer> waterConsumptionCalculator(List<WaterIndexStatement> indexStatements);
}
