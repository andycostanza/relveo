package com.andycostanza.relveo.utils;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class CsvFileMapperImpl implements CsvFileMapper {
    private final CsvMapper mapper = new CsvMapper();

    @Override
    public <T> List<T> read(Class<T> clazz, InputStream stream) throws IOException {
        mapper.findAndRegisterModules();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        CsvSchema schema = mapper.schemaFor(clazz)
                .withColumnSeparator(';')
                .withHeader()
                .withColumnReordering(true);
        ObjectReader reader = mapper.readerFor(clazz)
                .with(schema);
        return reader.<T>readValues(stream).readAll();
    }

    @Override
    public <T> byte[] write(Class<T> clazz, List<T> list) throws IOException {
        mapper.findAndRegisterModules();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        CsvSchema schema = mapper.schemaFor(clazz)
                .withColumnSeparator(';')
                .withHeader();
        ObjectWriter writer = mapper.writerFor(clazz)
                .with(schema);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        writer.writeValues(outputStream).writeAll(list);
        return outputStream.toByteArray();
    }
}
