import {Component, TemplateRef} from '@angular/core';
import {NotificationService} from "../service/notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  host: {'[class.ngb-toasts]': 'true'}
})
export class NotificationComponent {

  constructor(public notificationService: NotificationService) {}

  isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }

}
