package com.andycostanza.relveo.water;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by andy on 24/03/17.
 */
@Repository
public interface WaterIndexStatementRepository extends JpaRepository<WaterIndexStatement, Long> {
    List<WaterIndexStatement> findTop53ByUserIdOrderByStatementDateDesc(String userId);

    Page<WaterIndexStatement> findByUserId(String userId, Pageable page);

    List<WaterIndexStatement> findByUserIdOrderByStatementDateDesc(String userId);

    Optional<WaterIndexStatement> findByUserIdAndStatementDate(String userId, LocalDate statementDate);
}
