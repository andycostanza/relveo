package com.andycostanza.relveo.userpreference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserPreferenceControllerTest {
    UserPreferenceController controller;
    @Mock
    UserPreferenceRepository repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new UserPreferenceController(repository);
    }

    @Test
    @DisplayName("should insert a new UserPreference in database if the userId not found in persistence")
    void upsertUserPreferenceTest10() {
        //given
        Mockito.when(repository.findById(Mockito.anyString()))
                .thenReturn(Optional.empty());
        //when
        ResponseEntity result = controller.upsertUserPreference("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200, result.getStatusCodeValue())
        );
        Mockito.verify(repository,
                       Mockito.times(1))
                .save(Mockito.any(UserPreference.class));
    }

    @Test
    @DisplayName("should update UserPreference if it founded in persistence with userId")
    void upsertUserPreferenceTest20() {
        //given
        UserPreference userPreference = UserPreference.builder()
                .build();
        Mockito.when(repository.findById(Mockito.anyString()))
                .thenReturn(Optional.of(userPreference));
        //when
        ResponseEntity result = controller.upsertUserPreference("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200, result.getStatusCodeValue())
        );
        Mockito.verify(repository,
                       Mockito.times(1))
                .save(Mockito.any(UserPreference.class));
    }
}