package com.andycostanza.relveo.electricity;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ElectricityIndexStatementMapperImpl implements ElectricityIndexStatementMapper {
    private final ElectricityIndexStatementRepository repository;

    @Override
    public List<ElectricityIndexStatement> toEntities(List<ElectricityIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(electricityIndexStatementCsv -> {
                    Optional<ElectricityIndexStatement> entity = repository.findByUserIdAndStatementDate(userId, electricityIndexStatementCsv.getStatementDate());
                    if (entity.isPresent()){
                        entity.get().setDayIndex(electricityIndexStatementCsv.getDayIndex());
                        entity.get().setNightIndex(electricityIndexStatementCsv.getNightIndex());
                        return entity.get();
                    }else{
                        return ElectricityIndexStatement.builder()
                                .statementDate(electricityIndexStatementCsv.getStatementDate())
                                .dayIndex(electricityIndexStatementCsv.getDayIndex())
                                .nightIndex(electricityIndexStatementCsv.getNightIndex())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<ElectricityIndexStatementCsv> toCsv(List<ElectricityIndexStatement> entities) {
        return entities.stream()
                .map(electricityIndexStatement -> ElectricityIndexStatementCsv.builder()
                        .statementDate(electricityIndexStatement.getStatementDate())
                        .dayIndex(electricityIndexStatement.getDayIndex())
                        .nightIndex(electricityIndexStatement.getNightIndex())
                        .build())
                .collect(Collectors.toList());
    }
}
