package com.andycostanza.relveo.userpreference;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserPreference implements Serializable {
    @Id
    private String id;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate lastLogin;
    private LocalTime lastLoginTime;
    @Enumerated(EnumType.STRING)
    private TankType tankType;
    private BigDecimal widthOfTank;
    private BigDecimal lengthOfTank;
    private BigDecimal diameterOfTank;

    @Transient
    public Optional<BigDecimal> getSurfaceOfTank(){
        Assert.notNull(this.widthOfTank, "The width of tank should not be null to calculate the surface");
        Assert.notNull(this.lengthOfTank, "The length of tank should not be null to calculate the surface");
        Assert.notNull(this.tankType, "The type of tank should not be null to calculate the surface");
        if(TankType.CUBOID.equals(this.tankType)){
            return Optional.of(this.widthOfTank.multiply(this.lengthOfTank));
        }else{
            //not yet implemented
            return Optional.empty();
        }
    }


}
