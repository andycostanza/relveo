package com.andycostanza.relveo.electricity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("ElectricityIndexStatementMapper")
class ElectricityIndexStatementMapperImplTest {
    ElectricityIndexStatementMapper mapper;
    @Mock
    ElectricityIndexStatementRepository repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new ElectricityIndexStatementMapperImpl(repository);
    }

    @Test
    @DisplayName("should return new ElectricityIndexStatement if it doesn't find in the repository")
    void toEntitiesTest10() {
        //given
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.empty());
        //when
        LocalDate now = LocalDate.now();
        ElectricityIndexStatementCsv csv = ElectricityIndexStatementCsv.builder()
                .statementDate(now)
                .dayIndex(BigDecimal.ONE)
                .nightIndex(BigDecimal.TEN)
                .build();
        List<ElectricityIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertNull(result.get(0)
                                           .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getDayIndex()),
                  () -> assertEquals(BigDecimal.TEN,
                                     result.get(0)
                                             .getNightIndex()),
                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return an updated existing ElectricityIndexStatement if it find in the repository")
    void toEntitiesTest20() {
        //given
        LocalDate now = LocalDate.now();
        ElectricityIndexStatement electricityIndexStatement = ElectricityIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .dayIndex(BigDecimal.ZERO)
                .nightIndex(BigDecimal.ZERO)
                .statementDate(now)
                .build();
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.of(electricityIndexStatement));
        //when

        ElectricityIndexStatementCsv csv = ElectricityIndexStatementCsv.builder()
                .statementDate(now)
                .dayIndex(BigDecimal.ONE)
                .nightIndex(BigDecimal.TEN)
                .build();
        List<ElectricityIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(1L,
                                     result.get(0)
                                             .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getDayIndex()),
                  () -> assertEquals(BigDecimal.TEN,
                                     result.get(0)
                                             .getNightIndex()),
                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return a List of ElectricityIndexStatementCsv")
    void toCsvTest10() {
        //given
        LocalDate now = LocalDate.now();
        ElectricityIndexStatement electricityIndexStatement = ElectricityIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .dayIndex(BigDecimal.ONE)
                .nightIndex(BigDecimal.TEN)
                .statementDate(now)
                .build();
        //when
        List<ElectricityIndexStatementCsv> result = mapper.toCsv(Collections.singletonList(electricityIndexStatement));
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getDayIndex()),
                  () -> assertEquals(BigDecimal.TEN,
                                     result.get(0)
                                             .getNightIndex()));
    }
}