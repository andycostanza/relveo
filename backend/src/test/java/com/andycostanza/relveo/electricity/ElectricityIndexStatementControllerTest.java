package com.andycostanza.relveo.electricity;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.utils.CsvFileMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("ElectricityIndexStatement controller")
class ElectricityIndexStatementControllerTest {
    ElectricityIndexStatementController controller;
    @Mock
    ElectricityIndexStatementRepository repository;
    @Mock
    ChartService chartService;
    @Mock
    CsvFileMapper csvFileMapper;
    @Mock
    ElectricityIndexStatementMapper mapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new ElectricityIndexStatementController(repository,
                                                             chartService,
                                                             csvFileMapper,
                                                             mapper);
    }

    @Test
    @DisplayName("should findAllPaginate method retrieve data in repository and return a body with HTTP 200")
    void findAllPaginateTest10() {
        //given
        Mockito.when(repository.findByUserId(Mockito.anyString(),
                                             Mockito.any(PageRequest.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(ElectricityIndexStatement.builder()
                                                                             .id(1L)
                                                                             .build())));
        //when
        ResponseEntity result = controller.findAllPaginate("userId",
                                                           1,
                                                           1);
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .findByUserId(Mockito.anyString(),
                              Mockito.any(PageRequest.class));
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNotNull(result.getBody()),
                  () -> assertEquals(200,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should buildChart method retrieve data in repository and return a body with HTTP 200")
    void buildChartTest10() {
        //given
        Mockito.when(repository.findTop53ByUserIdOrderByStatementDateDesc("userId"))
                .thenReturn(Collections.singletonList(ElectricityIndexStatement.builder()
                                                              .id(1L)
                                                              .build()));
        Mockito.when(chartService.electricityConsumptionCalculator(Mockito.anyList()))
                .thenReturn(Collections.singletonList(ChartContainer.builder()
                                                              .name("chartContainer")
                                                              .build()));

        //when
        ResponseEntity result = controller.buildChart("userId");

        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .findTop53ByUserIdOrderByStatementDateDesc("userId");
        Mockito.verify(chartService,
                       Mockito.times(1))
                .electricityConsumptionCalculator(Mockito.anyList());
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNotNull(result.getBody()),
                  () -> assertEquals(200,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should findOne method retrieve data in repository and return a body with HTTP 200")
    void findOneTest10() {
        //given
        Mockito.when(repository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(ElectricityIndexStatement.builder()
                                                .id(1L)
                                                .build()));
        //when
        ResponseEntity result = controller.findOne(1L);
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .findById(Mockito.anyLong());
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNotNull(result.getBody()),
                  () -> assertEquals(200,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should create method save data in repository and return a body with HTTP 200")
    void createTest10() {
        //given
        Mockito.when(repository.save(Mockito.any(ElectricityIndexStatement.class)))
                .thenReturn(ElectricityIndexStatement.builder()
                                    .id(1L)
                                    .build());
        //when
        ResponseEntity result = controller.create(ElectricityIndexStatement.builder()
                                                          .build());
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .save(Mockito.any(ElectricityIndexStatement.class));
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNotNull(result.getBody()),
                  () -> assertEquals(200,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should update method update data in repository and return a body with HTTP 200 ")
    void updateTest10() {
        //given
        Mockito.when(repository.save(Mockito.any(ElectricityIndexStatement.class)))
                .thenReturn(ElectricityIndexStatement.builder()
                                    .id(1L)
                                    .build());
        //when
        ResponseEntity result = controller.update(ElectricityIndexStatement.builder()
                                                          .build());
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .save(Mockito.any(ElectricityIndexStatement.class));
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNotNull(result.getBody()),
                  () -> assertEquals(200,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should delete method erase data in repository and return HTTP 204 with empty body")
    void deleteTest10() {
        //given
        Mockito.doNothing()
                .when(repository)
                .deleteById(Mockito.anyLong());
        //when
        ResponseEntity result = controller.delete(1L);
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .deleteById(Mockito.anyLong());
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertNull(result.getBody()),
                  () -> assertEquals(204,
                                     result.getStatusCode()
                                             .value()));
    }

    @Test
    @DisplayName("should call deleteInBatch en return http 204")
    void wipeTest10() {
        //given

        //when    
        ResponseEntity result = controller.wipe("userId");
        //then
        Mockito.verify(repository,
                       Mockito.times(1))
                .findByUserIdOrderByStatementDateDesc(Mockito.anyString());
        Mockito.verify(repository,
                       Mockito.times(1))
                .deleteInBatch(Mockito.anyIterable());
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(204,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should throw an IOException en return http 500")
    void exportCsvTest10() throws Exception {
        //given
        Mockito.doThrow(IOException.class)
                .when(csvFileMapper)
                .write(Mockito.eq(ElectricityIndexStatementCsv.class),
                       Mockito.anyList());
        //when    
        ResponseEntity result = controller.exportCsv("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(500,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 200 with a body")
    void exportCsvTest20() throws Exception {
        //given
        Mockito.when(csvFileMapper.write(Mockito.eq(ElectricityIndexStatementCsv.class),
                                         Mockito.anyList()))
                .thenReturn("csv;file".getBytes());
        LocalDate now = LocalDate.now();
        Mockito.when(repository.findByUserIdOrderByStatementDateDesc(Mockito.anyString()))
                .thenReturn(Collections.singletonList(ElectricityIndexStatement.builder()
                                                              .statementDate(now)
                                                              .build()));
        Mockito.when(mapper.toCsv(Mockito.anyList()))
                .thenReturn(Collections.singletonList(ElectricityIndexStatementCsv.builder()
                                                              .statementDate(now)
                                                              .build()));
        //when
        ResponseEntity result = controller.exportCsv("userId");
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200,
                                     result.getStatusCodeValue()),
                  () -> assertNotNull(result.getBody()));
    }

    @Test
    @DisplayName("should throw an IOException en return http 500")
    void importMultipartTest10() throws Exception {
        //given
        Mockito.doThrow(IOException.class)
                .when(csvFileMapper)
                .read(Mockito.eq(ElectricityIndexStatementCsv.class),
                      Mockito.any(InputStream.class));
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "csv;file".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(500,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 411 if file is empty")
    void importMultipartTest20() throws Exception {
        //given
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(411,
                                     result.getStatusCodeValue()));
    }

    @Test
    @DisplayName("should return http 200 with a body")
    void importMultipartTest30() throws Exception {
        //given
        Mockito.when(csvFileMapper.read(Mockito.eq(ElectricityIndexStatementCsv.class),
                                        Mockito.any(InputStream.class)))
                .thenReturn(Collections.singletonList(ElectricityIndexStatementCsv.builder()
                                                              .statementDate(LocalDate.now())
                                                              .build()));
        //when
        ResponseEntity result = controller.importMultipart("userId",
                                                           new MockMultipartFile("data",
                                                                                 "",
                                                                                 "text/csv",
                                                                                 "csv;file".getBytes()));
        //then
        assertAll("response",
                  () -> assertNotNull(result),
                  () -> assertEquals(200,
                                     result.getStatusCodeValue()),
                  () -> assertNotNull(result.getBody())
        );
    }
}