package com.andycostanza.relveo.water;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
@RequiredArgsConstructor
public class WaterIndexStatementMapperImpl implements WaterIndexStatementMapper {
    private final WaterIndexStatementRepository repository;
    @Override
    public List<WaterIndexStatement> toEntities(List<WaterIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(waterIndexStatementCsv -> {
                    Optional<WaterIndexStatement> entity = repository.findByUserIdAndStatementDate(userId, waterIndexStatementCsv.getStatementDate());
                    if (entity.isPresent()){
                        entity.get().setWaterIndex(waterIndexStatementCsv.getWaterIndex());
                        return entity.get();
                    }else{
                        return WaterIndexStatement.builder()
                                .statementDate(waterIndexStatementCsv.getStatementDate())
                                .waterIndex(waterIndexStatementCsv.getWaterIndex())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<WaterIndexStatementCsv> toCsv(List<WaterIndexStatement> entities) {
        return entities.stream()
                .map(waterIndexStatement -> WaterIndexStatementCsv.builder()
                        .statementDate(waterIndexStatement.getStatementDate())
                        .waterIndex(waterIndexStatement.getWaterIndex())
                        .build())
                .collect(Collectors.toList());
    }
}
