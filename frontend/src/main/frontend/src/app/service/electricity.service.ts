/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Electricity} from "../model/electricity";
import {Page} from "../model/page";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ElectricityService {
  private url = '/api/electricityIndexStatements';
  multi: any[] = [];

  electricityList: Electricity[] = [];
  electricity: Electricity = new Electricity();
  itemsPerPage: number = 10;
  totalItems: any;
  page: any = 1;
  previousPage: any;

  constructor(private http: HttpClient) {
  }

  getUrl(): string {
    return this.url
  }

  getChart(userId: string) {
    return this.http.get<any>(this.url + '/chart?userId=' + userId)
      .subscribe(list => this.multi = list);
  }

  getAllPaginate(userId: string, page: number, size: number) {
    return this.http.get<Page>(this.url + '?userId=' + userId + '&size=' + size + '&page=' + page)
      .subscribe(
        response => {
          this.electricityList = response.content;
          this.totalItems = response.totalElements;
          this.itemsPerPage = response.size;
        });
  }

  getOne(id: number) {
    const url = `${this.url}/${id}`;
    return this.http.get<Electricity>(url)
      .subscribe(statementFounded => this.electricity = statementFounded);
  }

  //////// Save methods //////////

  /** POST: add a new electricityForm to the server */
  add(electricity: Electricity) {
    return this.http.post<Electricity>(this.url, electricity, httpOptions)
      .subscribe(statementSaved => this.electricityList.unshift(statementSaved));
  }

  /** DELETE: delete the electricity from the server */
  delete(electricity: Electricity | number) {
    const id = typeof electricity === 'number' ? electricity : electricity.id;
    const url = `${this.url}/${id}`;

    return this.http.delete<Electricity>(url, httpOptions)
      .subscribe(() => this.electricityList = this.electricityList.filter(e => e !== electricity));
  }

  /** PUT: update the electricity on the server */
  update(electricity: Electricity) {
    return this.http.put<Electricity>(this.url, electricity, httpOptions)
      .subscribe(statementUpdated => {
        let itemIndex = this.electricityList.findIndex(item => item.id == electricity.id);
        this.electricityList[itemIndex] = statementUpdated;
      });
  }
}
