package com.andycostanza.relveo.heating;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HeatingIndexStatementCsv implements Serializable {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate statementDate;
    private BigDecimal heightOfTank;
}
