package com.andycostanza.relveo.water;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class WaterIndexStatementMapperImplTest {

    WaterIndexStatementMapper mapper;
    @Mock
    WaterIndexStatementRepository repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new WaterIndexStatementMapperImpl(repository);
    }

    @Test
    @DisplayName("should return new WaterIndexStatement if it doesn't find in the repository")
    void toEntitiesTest10() {
        //given
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.empty());
        //when
        LocalDate now = LocalDate.now();
        WaterIndexStatementCsv csv = WaterIndexStatementCsv.builder()
                .statementDate(now)
                .waterIndex(BigDecimal.ONE)
                .build();
        List<WaterIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertNull(result.get(0)
                                           .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getWaterIndex()),
                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return an updated existing WaterIndexStatement if it find in the repository")
    void toEntitiesTest20() {
        //given
        LocalDate now = LocalDate.now();
        WaterIndexStatement waterIndexStatement = WaterIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .waterIndex(BigDecimal.ZERO)
                .statementDate(now)
                .build();
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(),
                                                             Mockito.any(LocalDate.class)))
                .thenReturn(Optional.of(waterIndexStatement));
        //when

        WaterIndexStatementCsv csv = WaterIndexStatementCsv.builder()
                .statementDate(now)
                .waterIndex(BigDecimal.ONE)
                .build();
        List<WaterIndexStatement> result = mapper.toEntities(Collections.singletonList(csv),
                                                                   "userId");
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(1L,
                                     result.get(0)
                                             .getId()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getWaterIndex()),
                  () -> assertEquals("userId",
                                     result.get(0)
                                             .getUserId())
        );
    }

    @Test
    @DisplayName("should return a List of WaterIndexStatementCsv")
    void toCsvTest10() {
        //given
        LocalDate now = LocalDate.now();
        WaterIndexStatement waterIndexStatement = WaterIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .waterIndex(BigDecimal.ONE)
                .statementDate(now)
                .build();
        //when
        List<WaterIndexStatementCsv> result = mapper.toCsv(Collections.singletonList(waterIndexStatement));
        //then
        assertAll("result",
                  () -> assertNotNull(result),
                  () -> assertEquals(1,
                                     result.size()),
                  () -> assertEquals(now,
                                     result.get(0)
                                             .getStatementDate()),
                  () -> assertEquals(BigDecimal.ONE,
                                     result.get(0)
                                             .getWaterIndex()));
    }
}