package com.andycostanza.relveo.water;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by andy on 24/03/17.
 */
@Entity
@Table(indexes = {@Index(columnList = "userId", name = "water_userid_idx")})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WaterIndexStatement implements Serializable,Comparable<WaterIndexStatement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /*(TemporalType.DATE)*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate statementDate;
    private BigDecimal waterIndex;
    private String userId;

    @Override
    public int compareTo(WaterIndexStatement w) {
        return getStatementDate().compareTo(w.getStatementDate());
    }
}
