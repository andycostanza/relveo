/*
 * Relveo is a Spring Boot backend with an embed Angular Frontend made for simplify calculation of everyday energy consumption.
 * Copyright (c) 2018. Andy Costanza <contact@andycostanza.com>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Heating} from "../model/heating";
import {Page} from "../model/page";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class HeatingService {
  private url = '/api/heatingIndexStatements';
  multi: any[] = [];

  heatingList: Heating[] = [];
  heating: Heating = new Heating();
  itemsPerPage: number=10;
  totalItems: any;
  page: any=1;
  previousPage: any;
  constructor(private http: HttpClient) {
  }

  getUrl(): string {
    return this.url
  }

  getChart(userId: string) {
    return this.http.get<any>(this.url + '/chart?userId=' + userId)
      .subscribe(list => this.multi = list);
  }

  getAllPaginate(userId: string, page: number, size: number) {
    return this.http.get<Page>(this.url + '?userId=' + userId + '&size=' + size + '&page=' + page)
      .subscribe(
        response => {
          this.heatingList = response.content;
          this.totalItems = response.totalElements;
          this.itemsPerPage = response.size;
        });
  }

  getOne(id: number) {
    const url = `${this.url}/${id}`;
    return this.http.get<Heating>(url)
      .subscribe(statementFounded => this.heating = statementFounded);
  }

  //////// Save methods //////////

  /** POST: add a new heatingForm to the server */
  add(heating: Heating) {
    return this.http.post<Heating>(this.url, heating, httpOptions)
      .subscribe(statementSaved => this.heatingList.unshift(statementSaved));
  }

  /** DELETE: delete the heating from the server */
  delete(heating: Heating | number){
    const id = typeof heating === 'number' ? heating : heating.id;
    const url = `${this.url}/${id}`;

    return this.http.delete<Heating>(url, httpOptions)
      .subscribe(() => this.heatingList = this.heatingList.filter(e => e !== heating));
  }

  /** PUT: update the heating on the server */
  update(heating: Heating){
    return this.http.put<Heating>(this.url, heating, httpOptions)
      .subscribe(statementUpdated => {
      let itemIndex = this.heatingList.findIndex(item => item.id == heating.id);
      this.heatingList[itemIndex] = statementUpdated;
    });
  }
}
