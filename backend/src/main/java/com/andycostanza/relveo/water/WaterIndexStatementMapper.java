package com.andycostanza.relveo.water;

import java.io.Serializable;
import java.util.List;

public interface WaterIndexStatementMapper extends Serializable {
    List<WaterIndexStatement> toEntities(List<WaterIndexStatementCsv> csv, String userId);

    List<WaterIndexStatementCsv> toCsv(List<WaterIndexStatement> entities);
}
